#!/bin/bash
if [ -e "env.sh" ]; then
	source env.sh
elif [ -e "../env.sh" ]; then
	source ../env.sh
elif [ -e "./mvnw" ]; then
	echo "env.sh missing, using \"./mvnw\""
	MVN="./mvnw"
elif [ -e ../mvnw ]; then
	echo "env.sh missing, using ../mvnw"
	MVN="../mvnw"
else
	echo "env.sh missing, using \"mvn\""
	MVN="mvn"
fi

# ./debug.sh | tee audio-web-services.log

# -Dspring-boot.run.jvmArguments="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5005"
# ./mvnw spring-boot:run -Dspring.config.additional-location=$HOME/Projects-adrhc/audio-web-services/config/ --debug

# $HOME/.sdkman/candidates/java/current/bin/java --enable-preview -jar /fast-disk/java-apps/audio-web-services-1.0-SNAPSHOT.jar --spring.config.additional-location=$HOME/Projects-adrhc/audio-web-services/config/

# config/application.yml is automatically detected when running from $HOME/Projects-adrhc/audio-web-services
#cd $HOME/Projects-adrhc/audio-web-services

./mvnw spring-boot:run

package ro.go.adrhc.audiodbshell.shell.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;

import static org.springframework.util.StringUtils.hasText;
import static ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory.createDiskLocation;

@Component
public class StringToDiskLocation implements Converter<String, DiskLocation> {
	@Override
	public DiskLocation convert(String path) {
		return hasText(path) ? createDiskLocation(path) : null;
	}
}

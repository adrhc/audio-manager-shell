package ro.go.adrhc.audiodbshell.shell.songssearch;

import lombok.extern.slf4j.Slf4j;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import org.springframework.util.Assert;
import ro.go.adrhc.audiodbshell.shell.AbstractCommands;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocationAccessors;
import ro.go.adrhc.audiomanager.domain.location.LocationType;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;
import ro.go.adrhc.audiomanager.managers.SongsSearchManager;
import ro.go.adrhc.audiomanager.util.stringifier.PlEntryStringifier;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.FileSystemIndex;

import java.io.IOException;
import java.net.URI;
import java.util.function.Function;

import static org.springframework.shell.standard.ShellOption.NULL;
import static org.springframework.util.StringUtils.hasText;
import static ro.go.adrhc.audiomanager.domain.location.LocationType.DISK;
import static ro.go.adrhc.audiomanager.domain.location.LocationType.YOUTUBE;
import static ro.go.adrhc.util.text.StringUtils.concat;

@ShellComponent
@Slf4j
public class SongsSearchCommands extends AbstractCommands {
	private final SongsSearchManager songsSearchManager;
	private final PlEntryStringifier plEntryStringifier;

	public SongsSearchCommands(AppPaths appPaths,
			FileSystemIndex<URI, AudioMetadata> amIndexRepository,
			PlEntryStringifier plEntryStringifier,
			SongsSearchManager songsSearchManager) {
		super(appPaths, amIndexRepository);
		this.plEntryStringifier = plEntryStringifier;
		this.songsSearchManager = songsSearchManager;
	}

	@ShellMethod("Search everywhere for a song with free text criteria.")
	public void search(String text) throws IOException {
		Assert.isTrue(hasText(text), "Must contain not empty text!");
		searchYouTubeVideos("", "", text);
		searchOnDisk("", "", text);
	}

	@ShellMethod("Search everywhere for a song with structured criteria.")
	public void structuredSearch(@ShellOption(defaultValue = NULL) String title,
			@ShellOption(defaultValue = NULL) String artist,
			@ShellOption(defaultValue = NULL) String text) throws IOException {
		searchYouTubeVideos(title, artist, text);
		searchOnDisk(title, artist, text);
	}

	@ShellMethod(value = "Search the file system for a song.")
	public void searchOnDisk(@ShellOption(defaultValue = NULL) String title,
			@ShellOption(defaultValue = NULL) String artist,
			@ShellOption(defaultValue = NULL) String text) throws IOException {
		PlaylistEntries playlistEntries = songsSearchManager.searchOnDisk(title, artist, text);
		print(DISK, DiskLocationAccessors::rawPath, playlistEntries);
	}

	@ShellMethod(value = "Search YouTube videos.", key = "search-yt-videos")
	public void searchYouTubeVideos(@ShellOption(defaultValue = NULL) String title,
			@ShellOption(defaultValue = NULL) String artist,
			@ShellOption(defaultValue = NULL) String text) {
		PlaylistEntries playlistEntries = songsSearchManager.searchYouTubeVideos(title, artist,
				text);
		print(YOUTUBE, plEntryStringifier::ytPlEntryString, playlistEntries);
	}

	@ShellMethod(value = "Search YouTube music.", key = "search-yt-music")
	public void searchYouTubeMusic(@ShellOption(defaultValue = NULL) String title,
			@ShellOption(defaultValue = NULL) String artist,
			@ShellOption(defaultValue = NULL) String text) {
		PlaylistEntries playlistEntries = songsSearchManager.searchYouTubeMusic(title, artist,
				text);
		print(YOUTUBE, plEntryStringifier::ytPlEntryString, playlistEntries);
	}

	private static void print(LocationType searchedType,
			Function<PlaylistEntry, String> plEntryToString,
			PlaylistEntries playlistEntries) {
		if (playlistEntries.isEmpty()) {
			log.debug("\nFound nothing!");
		} else {
			log.debug("\n{}\nFound {} items at {}.",
					concat(plEntryToString, playlistEntries),
					playlistEntries.size(), searchedType);
		}
	}
}

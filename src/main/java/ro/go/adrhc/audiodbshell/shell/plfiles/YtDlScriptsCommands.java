package ro.go.adrhc.audiodbshell.shell.plfiles;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import ro.go.adrhc.audiodbshell.shell.AbstractCommands;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.managers.YtDlBashScriptsManager;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.FileSystemIndex;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;

import static ro.go.adrhc.audiomanager.config.context.AppPathType.INDEX;
import static ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory.createDiskLocation;

@ShellComponent
public class YtDlScriptsCommands extends AbstractCommands {
	private final YtDlBashScriptsManager ytDlBashScriptsManager;

	public YtDlScriptsCommands(AppPaths appPaths,
			FileSystemIndex<URI, AudioMetadata> amIndexRepository,
			YtDlBashScriptsManager ytDlBashScriptsManager) {
		super(appPaths, amIndexRepository);
		this.ytDlBashScriptsManager = ytDlBashScriptsManager;
	}

	/**
	 * yt-dl "Youtube likes copy.m3u8"
	 * yt-dl test.m3u8 2
	 */
	@ShellMethod(value = "Create scripts to download youtube missing songs from a playlist.",
			key = {"ytdlsc", "youtube-download-script-create"})
	public void createYtDownloadScripts(Path ytPlaylistPath,
			@ShellOption(defaultValue = "10") int partitionsSize) throws IOException {
		pathTypesValidator(INDEX).addPlaylist(ytPlaylistPath).throwIfInvalid();
		ytDlBashScriptsManager.create(createDiskLocation(ytPlaylistPath), partitionsSize);
	}

	/**
	 * rm-yt-dl test.m3u8
	 */
	@ShellMethod(value = "Remove youtube download scripts.",
			key = {"ytdlsrm", "youtube-download-script-remove"})
	public void removeDownloadScripts(Path playlistPath) {
		ytDlBashScriptsManager.remove(createDiskLocation(playlistPath));
	}
}

package ro.go.adrhc.audiodbshell.shell.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.nio.file.Path;

//@ConfigurationPropertiesBinding
@Component
public class StringToPathConverter implements Converter<String, Path> {
	@Override
	public Path convert(@NonNull String source) {
		return Path.of(source);
	}
}

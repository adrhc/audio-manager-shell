package ro.go.adrhc.audiodbshell.shell.plfiles;

import lombok.extern.slf4j.Slf4j;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import ro.go.adrhc.audiodbshell.shell.AbstractPlaylistsBrowser;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.datasources.disk.playlists.DiskPlaylistsRepository;
import ro.go.adrhc.audiomanager.infrastructure.backup.BackupResult;
import ro.go.adrhc.audiomanager.infrastructure.backup.BackupService;
import ro.go.adrhc.audiomanager.infrastructure.backup.RestorationResult;
import ro.go.adrhc.audiomanager.util.stringifier.PlaylistsStringifier;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.FileSystemIndex;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;
import java.util.List;

import static org.springframework.shell.standard.ShellOption.NULL;
import static ro.go.adrhc.audiomanager.config.context.AppPathType.BACKUPS;
import static ro.go.adrhc.audiomanager.config.context.AppPathType.PLAYLISTS;
import static ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory.createDiskLocation;

@ShellComponent
@Slf4j
public class BackupCommands extends AbstractPlaylistsBrowser {
	private final BackupService backupService;

	public BackupCommands(AppPaths appPaths,
			FileSystemIndex<URI, AudioMetadata> amIndexRepository,
			DiskPlaylistsRepository diskPlaylistsRepository,
			PlaylistsStringifier playlistsStringifier, BackupService backupService) {
		super(appPaths, amIndexRepository,
				diskPlaylistsRepository, playlistsStringifier);
		this.backupService = backupService;
	}

	@ShellMethod(value = "Create a playlist(s) backup.")
	public void backup(@ShellOption(defaultValue = NULL) Path playlistPath) throws IOException {
		if (playlistPath == null) {
			throwIfInvalid(BACKUPS, PLAYLISTS);
			logFailedBackups(backupService.backup());
			showBackups();
			log.debug("\nAll playlists were backed up!");
		} else {
			pathTypesValidator(BACKUPS).addPlaylist(playlistPath).throwIfInvalid();
			backupService.backup(createDiskLocation(playlistPath))
					.ifPresentOrElse(this::logBackupResult,
							() -> log.debug("\nBackup failed to start!"));
		}
	}

	@ShellMethod(value = "Restore a playlist(s) backup.")
	public void restore(@ShellOption(defaultValue = NULL) Path backupPath) throws IOException {
		if (backupPath == null) {
			throwIfInvalid(PLAYLISTS, BACKUPS);
			logFailedRestorations(backupService.restore());
			showPlaylists();
			log.debug("\nAll backups were restored!");
		} else {
			pathTypesValidator(PLAYLISTS).addBackup(backupPath).throwIfInvalid();
			backupService.restore(createDiskLocation(backupPath))
					.ifPresentOrElse(this::logRestorationResult,
							() -> log.debug("\nRestoration failed to start!"));
		}
	}

	private void logBackupResult(BackupResult backupResult) {
		if (backupResult.isBackupMissing()) {
			log.debug("\nFailed to backup!");
		} else {
			log.debug("\nBackup created at: {}", backupResult.backup());
		}
	}

	private void logRestorationResult(RestorationResult restorationResult) {
		if (restorationResult.isRestoredMissing()) {
			log.debug("\nFailed to restore!");
		} else {
			log.debug("\nRestored at: {}", restorationResult.restored());
		}
	}

	private void logFailedBackups(List<BackupResult> backupResults) {
		backupResults.stream()
				.filter(BackupResult::isBackupMissing)
				.forEach(br -> log.debug("\nFailed to backup: {}", br.path()));
	}

	private void logFailedRestorations(List<RestorationResult> restorationResults) {
		restorationResults.stream()
				.filter(RestorationResult::isRestoredMissing)
				.forEach(rr -> log.debug("\nFailed to restore: {}", rr.backup()));
	}
}

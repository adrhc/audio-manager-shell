package ro.go.adrhc.audiodbshell.shell;

import lombok.extern.slf4j.Slf4j;
import ro.go.adrhc.audiomanager.config.context.AppPathType;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.datasources.disk.playlists.DiskPlaylistsRepository;
import ro.go.adrhc.audiomanager.domain.playlist.Playlist;
import ro.go.adrhc.audiomanager.util.stringifier.PlaylistsStringifier;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.FileSystemIndex;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import static ro.go.adrhc.audiomanager.config.context.AppPathType.BACKUPS;
import static ro.go.adrhc.audiomanager.config.context.AppPathType.PLAYLISTS;

@Slf4j
public abstract class AbstractPlaylistsBrowser extends AbstractCommands {
	private static final String NO_RESULT = "Nothing was found!";
	protected final DiskPlaylistsRepository diskPlaylistsRepository;
	protected final PlaylistsStringifier playlistsStringifier;

	public AbstractPlaylistsBrowser(AppPaths appPaths,
			FileSystemIndex<URI, AudioMetadata> amIndexRepository,
			DiskPlaylistsRepository diskPlaylistsRepository,
			PlaylistsStringifier playlistsStringifier) {
		super(appPaths, amIndexRepository);
		this.diskPlaylistsRepository = diskPlaylistsRepository;
		this.playlistsStringifier = playlistsStringifier;
	}

	protected void showBackups() throws IOException {
		throwIfInvalid(BACKUPS);
		showPlaylists(BACKUPS, diskPlaylistsRepository.loadBackups());
	}

	protected void showPlaylists() throws IOException {
		throwIfInvalid(PLAYLISTS);
		showPlaylists(PLAYLISTS, diskPlaylistsRepository.loadPlaylists());
	}

	protected void showPlaylists(AppPathType appPathType,
			List<? extends Playlist<?>> playlists) {
		throwIfInvalid(PLAYLISTS);
		log.debug("\n{}", playlists.isEmpty()
				? NO_RESULT : playlistsStringifier.toString(appPathType, playlists));
	}
}

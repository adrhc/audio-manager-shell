package ro.go.adrhc.audiodbshell.shell.playlists.domain;

import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;

import java.util.List;
import java.util.stream.Collectors;

public interface YouTubeLocationsAware {
	static String stringify(YouTubeLocationsAware ytLocationsAware) {
		return ytLocationsAware.locations().stream()
				.map(YouTubeLocation::code)
				.collect(Collectors.joining(", "));
	}

	List<YouTubeLocation> locations();
}

package ro.go.adrhc.audiodbshell.shell.playlists;

import lombok.extern.slf4j.Slf4j;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import ro.go.adrhc.audiodbshell.shell.AbstractCommands;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.managers.DiskEntriesFixManager;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.FileSystemIndex;

import java.io.IOException;
import java.net.URI;

import static org.springframework.shell.standard.ShellOption.NULL;
import static ro.go.adrhc.audiomanager.config.context.AppPathType.INDEX;
import static ro.go.adrhc.audiomanager.config.context.AppPathType.PLAYLISTS;

@ShellComponent
@Slf4j
public class DiskEntriesFixCommands extends AbstractCommands {
	private final DiskEntriesFixManager diskEntriesFixManager;

	public DiskEntriesFixCommands(AppPaths appPaths,
			FileSystemIndex<URI, AudioMetadata> amIndexRepository,
			DiskEntriesFixManager diskEntriesFixManager) {
		super(appPaths, amIndexRepository);
		this.diskEntriesFixManager = diskEntriesFixManager;
	}

	@ShellMethod(value = "Create a new playlist (\"found\" and \"NOT found\") with the correct paths.",
			key = {"fix", "fix-paths"})
	public void fixDiskLocations(@ShellOption(defaultValue = NULL,
			value = "--playlist-path") DiskLocation plLocation) throws IOException {
		if (plLocation == null) {
			throwIfInvalid(INDEX, PLAYLISTS);
			diskEntriesFixManager.fixDiskLocations();
			log.debug("\nSong paths fixing on all playlists completed!");
		} else {
			pathTypesValidator(INDEX).addPlaylist(plLocation).throwIfInvalid();
			diskEntriesFixManager.fixDiskLocations(plLocation);
			log.debug("\nSong paths fixing for {} completed!", plLocation.rawPath());
		}
	}
}

package ro.go.adrhc.audiodbshell.shell.plcontext;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import ro.go.adrhc.audiomanager.datasources.youtube.config.YouTubeProperties;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytliked.YtLikedMusicMetadataProvider;
import ro.go.adrhc.audiomanager.infrastructure.filesystem.YtMusicAuthFileRepository;

import java.nio.file.Path;

import static org.springframework.shell.standard.ShellOption.NULL;

@ShellComponent("Manage the YouTube-Music authentication.")
@RequiredArgsConstructor
@Slf4j
public class YtMusicAuthMngmtCommands {
	private final YouTubeProperties youTubeProperties;
	private final YtLikedMusicMetadataProvider ytLikedMusicMetadataProvider;
	private final YtMusicAuthFileRepository ytMusicAuthFileRepository;

	@ShellMethod(value = "Show YouTube-Music plugin " +
			"authentication (aka /var/lib/mopidy/.config/auth.json).",
			key = {"showytmusicauth", "show-yt-music-auth"})
	public void showYtMusicAuth() {
		log.debug("\nYouTubeMusicAuthJson:\n{}", ytMusicAuthFileRepository.getYouTubeMusicAuth());
	}

	@SneakyThrows
	@ShellMethod(value = "Write YouTube-Music plugin authentication to the " +
			"specified path (by default is /var/lib/mopidy/.config/auth.json).",
			key = {"fixytmusicauth", "fix-yt-music-auth"})
	public void fixYtMusicAuth(@ShellOption(defaultValue = NULL) Path ytMusicAuthPath) {
		log.debug("\n{} updated!", ytMusicAuthFileRepository.writeYouTubeMusicAuth(ytMusicAuthPath));
	}

	@ShellMethod(value = "Check YouTube-Music plugin authentication validity.",
			key = {"checkytmusicauth", "check-yt-music-auth"})
	public void checkYtMusicAuth() {
		log.debug("""
						\nYouTube-Music plugin authentication is {}
						YouTube-Music plugin authentication HTTP headers source is:
						{}
						Update using "CURL (bash)" of YouTube Music -> Library -> Your Likes then RESTART Mopidy!""",
				ytLikedMusicMetadataProvider.isYouTubeMusicAuthValid() ? "VALID." : "INVALID!",
				youTubeProperties.getLikedMusicCurlPath());
	}
}

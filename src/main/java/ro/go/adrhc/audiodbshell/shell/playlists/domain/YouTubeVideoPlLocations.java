package ro.go.adrhc.audiodbshell.shell.playlists.domain;

import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;

import java.util.List;

import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory.ytVideoPlLocations;

public record YouTubeVideoPlLocations(List<YouTubeLocation> locations)
		implements YouTubeLocationsAware {
	public static YouTubeVideoPlLocations of(String... ytCodes) {
		return new YouTubeVideoPlLocations(ytVideoPlLocations(ytCodes));
	}

	@Override
	public String toString() {
		return YouTubeLocationsAware.stringify(this);
	}
}

package ro.go.adrhc.audiodbshell.shell.playlists.domain;

import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.config.MainProperties;
import ro.go.adrhc.audiomanager.config.plspec.PlaylistSpec;

import static org.springframework.util.StringUtils.hasText;

@Component
@RequiredArgsConstructor
public class StringToPlaylistSpecConverter implements Converter<String, PlaylistSpec> {
	private final MainProperties appProperties;

	@Override
	public PlaylistSpec convert(@NonNull String specName) {
		return hasText(specName) ? appProperties.getPlaylistSpec(specName) : null;
	}
}

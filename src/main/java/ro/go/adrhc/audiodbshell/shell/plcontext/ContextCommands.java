package ro.go.adrhc.audiodbshell.shell.plcontext;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import ro.go.adrhc.audiomanager.config.context.ObservableAppPaths;
import ro.go.adrhc.audiomanager.util.stringifier.AppPathsStringifier;

import java.nio.file.Path;
import java.util.stream.Stream;

import static org.springframework.shell.standard.ShellOption.NULL;

@ShellComponent("Manage the context paths and show the active configuration.")
@RequiredArgsConstructor
@Slf4j
public class ContextCommands {
	private final ApplicationContext ac;
	private final ObservableAppPaths observableAppPaths;
	private final AppPathsStringifier appPathsStringifier;

	@ShellMethod("Set (optionally) the paths of the used resources.")
	public void setPaths(@ShellOption(defaultValue = NULL) Path indexPath,
			@ShellOption(defaultValue = NULL) Path songsPath,
			@ShellOption(defaultValue = NULL) Path playlistsPath,
			@ShellOption(defaultValue = NULL) Path backupsPath,
			@ShellOption(defaultValue = NULL) Path scriptsPath) {
		observableAppPaths.update(indexPath, songsPath, playlistsPath, backupsPath, scriptsPath);
		log.debug("\n{}", appPathsStringifier.toString(observableAppPaths));
	}

	@ShellMethod(value = "Show the known paths.")
	public void showPaths() {
		log.debug("\n{}", appPathsStringifier.toString(observableAppPaths));
	}

	@ShellMethod("Show the application startup configuration.")
	public void showConfig() {
		configurationProperties().forEach(p -> log.debug("\n{}\n", p));
	}

	private Stream<?> configurationProperties() {
		return ac.getBeansWithAnnotation(ConfigurationProperties.class)
				.values().stream()
				.filter(p -> p.getClass().getName().startsWith("ro.go.adrhc.playlist"));
	}
}

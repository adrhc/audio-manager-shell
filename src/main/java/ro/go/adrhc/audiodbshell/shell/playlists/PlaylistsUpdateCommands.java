package ro.go.adrhc.audiodbshell.shell.playlists;

import lombok.extern.slf4j.Slf4j;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import ro.go.adrhc.audiodbshell.shell.AbstractCommands;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.playlists.PlaylistStorageOutcome;
import ro.go.adrhc.audiomanager.managers.PlaylistsManager;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.FileSystemIndex;

import java.io.IOException;
import java.net.URI;

import static org.springframework.shell.standard.ShellOption.NULL;
import static ro.go.adrhc.audiomanager.config.context.AppPathType.PLAYLISTS;
import static ro.go.adrhc.audiomanager.util.LogUtils.log;

@ShellComponent
@Slf4j
public class PlaylistsUpdateCommands extends AbstractCommands {
	private final PlaylistsManager playlistsManager;

	public PlaylistsUpdateCommands(AppPaths appPaths,
			FileSystemIndex<URI, AudioMetadata> amIndexRepository,
			PlaylistsManager playlistsManager) {
		super(appPaths, amIndexRepository);
		this.playlistsManager = playlistsManager;
	}

	@ShellMethod(key = {"creatediskpl"}, value = "Create a playlist from a directory.")
	public void createPlFromDirectory(@ShellOption(
			defaultValue = NULL, value = "--directory-path") DiskLocation dirLocation)
			throws IOException {
		throwIfInvalid(PLAYLISTS);
		log(playlistsManager.createPlFromDirectory(dirLocation));
	}

	@ShellMethod(key = {"fixtitles"}, value = "Update the playlist's entries' titles using file name or yt-title.")
	public void fixTitles(
			@ShellOption(defaultValue = NULL, value = "--playlist-path") DiskLocation plLocation,
			@ShellOption(defaultValue = "true") boolean missingTitleOnly) throws IOException {
		if (plLocation == null) {
			throwIfInvalid(PLAYLISTS);
			playlistsManager.fixTitles(missingTitleOnly);
			log.debug("\nTitles fixing on all playlists completed!");
		} else {
			throwIfInvalidPlDiskLocation(plLocation);
			playlistsManager.fixTitles(missingTitleOnly, plLocation);
			log.debug("\nTitles fixing for {} completed!", plLocation.rawPath());
		}
	}

	@ShellMethod(value = "Sum into with the differences between from and into playlists.")
	public void sumDiffs(@ShellOption(value = "--from-path") DiskLocation from,
			@ShellOption(value = "--into-path") DiskLocation into,
			@ShellOption(defaultValue = NULL,
					help = "the path of the playlist where the merge outcome goes",
					value = "--merge-path") DiskLocation mergedLocation) {
		pathTypesValidator(PLAYLISTS).addPlaylist(from).addPlaylist(into).throwIfInvalid();
		if (mergedLocation == null) {
			playlistsManager.sumDiffs(into, from);
			log.debug("\nMerged {}\ninto {}\nand saved to {}.", from, into, into);
		} else {
			playlistsManager.sumDiffs(mergedLocation, into, from);
			log.debug("\nMerged {}\ninto {}\nand saved to {}.", from, into,
					mergedLocation.rawPath());
		}
	}

	@ShellMethod(key = {"dedup"}, value = "Deduplicate playlists.")
	public void deduplicate(
			@ShellOption(defaultValue = NULL, value = "--playlist-path") DiskLocation plLocation)
			throws IOException {
		if (plLocation == null) {
			throwIfInvalid(PLAYLISTS);
			playlistsManager.deduplicate();
			log.debug("\nDeduplication completed!");
		} else {
			throwIfInvalidPlDiskLocation(plLocation);
			PlaylistStorageOutcome storageOutcome = playlistsManager.deduplicate(plLocation);
			log(storageOutcome);
		}
	}

	/**
	 * see PlaylistEntry.compareTo
	 */
	@ShellMethod(value = "Sort the playlist by title then location then duration.", key = {"sort-by-title-location-duration", "sort"})
	public void sort(
			@ShellOption(defaultValue = NULL, value = "--playlist-path") DiskLocation plLocation)
			throws IOException {
		if (plLocation == null) {
			throwIfInvalid(PLAYLISTS);
			playlistsManager.sort();
			log.debug("\nsorted all by comment");
		} else {
			throwIfInvalidPlDiskLocation(plLocation);
			playlistsManager.sort(plLocation);
			log.debug("\nsorted by title, path and duration: {}", plLocation.rawPath());
		}
	}
}

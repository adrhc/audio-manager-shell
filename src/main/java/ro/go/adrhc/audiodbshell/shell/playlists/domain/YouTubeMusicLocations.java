package ro.go.adrhc.audiodbshell.shell.playlists.domain;

import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;

import java.util.List;

import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory.ytMusicLocations;

public record YouTubeMusicLocations(List<YouTubeLocation> locations)
		implements YouTubeLocationsAware {
	public static YouTubeMusicLocations of(String... ytCodes) {
		return new YouTubeMusicLocations(ytMusicLocations(ytCodes));
	}

	@Override
	public String toString() {
		return YouTubeLocationsAware.stringify(this);
	}
}

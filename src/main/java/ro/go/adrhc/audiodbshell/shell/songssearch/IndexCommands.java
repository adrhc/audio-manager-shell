package ro.go.adrhc.audiodbshell.shell.songssearch;

import lombok.extern.slf4j.Slf4j;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import ro.go.adrhc.audiodbshell.shell.AbstractCommands;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.managers.AMIndexManager;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.FileSystemIndex;

import java.io.IOException;
import java.net.URI;

import static ro.go.adrhc.audiomanager.config.context.AppPathType.INDEX;

@ShellComponent
@Slf4j
public class IndexCommands extends AbstractCommands {
	private final AMIndexManager manager;

	public IndexCommands(AppPaths appPaths,
			FileSystemIndex<URI, AudioMetadata> amIndexRepository,
			AMIndexManager manager) {
		super(appPaths, amIndexRepository);
		this.manager = manager;
	}

	@ShellMethod(value = "Count indexed songs.")
	public void count() throws IOException {
		throwIfInvalid(INDEX);
		log.debug("\nThe index contains {} entries!", amIndexRepository.count());
	}

	@ShellMethod(key = {"create", "reset"},
			value = "Create the index at the provided path (remove it first, if existing).")
	public void reset() {
		try {
			manager.reset();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			log.debug("\n{} index reset failed!", getIndexPath());
		}
	}

	@ShellMethod(key = {"update", "shallow-disk-update"},
			value = "Update the index at the provided path.")
	public void shallowDiskUpdate() {
		try {
			manager.shallowDiskUpdate();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			log.debug("\n{} index update failed!", getIndexPath());
		}
	}
}

package ro.go.adrhc.audiodbshell.shell.playlists.domain;

import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;

import java.util.List;

import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory.ytVideoLocations;

public record YouTubeVideoLocations(List<YouTubeLocation> locations)
		implements YouTubeLocationsAware {
	public static YouTubeVideoLocations of(String... ytCodes) {
		return new YouTubeVideoLocations(ytVideoLocations(ytCodes));
	}

	@Override
	public String toString() {
		return YouTubeLocationsAware.stringify(this);
	}
}

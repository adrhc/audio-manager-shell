package ro.go.adrhc.audiodbshell.shell;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Lookup;
import ro.go.adrhc.audiomanager.config.context.AppPathType;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.config.context.validation.AppPathsValidator;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.FileSystemIndex;

import java.net.URI;
import java.nio.file.Path;

@Slf4j
public abstract class AbstractCommands {
	protected final AppPaths appPaths;
	protected final FileSystemIndex<URI, AudioMetadata> amIndexRepository;

	public AbstractCommands(AppPaths appPaths,
			FileSystemIndex<URI, AudioMetadata> amIndexRepository) {
		this.amIndexRepository = amIndexRepository;
		this.appPaths = appPaths;
	}

	protected Path getIndexPath() {
		return appPaths.getIndexPath();
	}

	protected void throwIfInvalidPlDiskLocation(DiskLocation diskPlLocation) {
		throwIfInvalidPlPath(diskPlLocation.path());
	}

	protected void throwIfInvalidPlPath(Path playlistPath) {
		contextPathsValidator().addPlaylist(playlistPath).throwIfInvalid();
	}

	protected void throwIfInvalid(AppPathType... appPathTypes) {
		pathTypesValidator(appPathTypes).throwIfInvalid();
	}

	protected AppPathsValidator pathTypesValidator(AppPathType... appPathTypes) {
		return contextPathsValidator().addCtxPathTypes(appPathTypes);
	}

	@Lookup
	protected AppPathsValidator contextPathsValidator() {
		return null;
	}
}

package ro.go.adrhc.audiodbshell.shell.plfiles;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import ro.go.adrhc.audiodbshell.shell.AbstractCommands;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.infrastructure.filesystem.plfiles.BackupFilesDirectory;
import ro.go.adrhc.audiomanager.infrastructure.filesystem.plfiles.playlistfiledirectory.PlaylistFilesDirectory;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.FileSystemIndex;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.springframework.shell.standard.ShellOption.NULL;
import static ro.go.adrhc.audiomanager.config.context.AppPathType.PLAYLISTS;

/**
 * Here is not used the main Model (the onse using Location instead of Path).
 */
@ShellComponent
@Slf4j
public class PlaylistsFileCommands extends AbstractCommands {
	private final PlaylistFilesDirectory playlistsFilesRepository;
	private final BackupFilesDirectory backupFilesRepository;

	public PlaylistsFileCommands(AppPaths appPaths,
			FileSystemIndex<URI, AudioMetadata> amIndexRepository,
			PlaylistFilesDirectory playlistsFilesRepository,
			BackupFilesDirectory backupFilesRepository) {
		super(appPaths, amIndexRepository);
		this.playlistsFilesRepository = playlistsFilesRepository;
		this.backupFilesRepository = backupFilesRepository;
	}

	/**
	 * apply-found
	 * apply-found "colinde - found.m3u8"
	 * <p>
	 * see <a href="https://docs.spring.io/spring-shell/docs/2.0.0.RELEASE/reference/htmlsingle/#_special_handling_of_boolean_parameters">...</a>
	 */
	@ShellMethod(value = "Use found-playlists to replace the corresponding ones.",
			key = {"apply-fix", "apply-found"})
	public void applyFound(@ShellOption(defaultValue = NULL) Path playlistPath) throws IOException {
		if (playlistPath == null) {
			throwIfInvalid(PLAYLISTS);
			playlistsFilesRepository.applyFound();
		} else {
			throwIfInvalidPlPath(playlistPath);
			playlistsFilesRepository.applyFound(playlistPath);
		}
	}

	@SneakyThrows
	@ShellMethod("Show a playlist's content.")
	public void cat(Path playlistPath) {
		throwIfInvalidPlPath(playlistPath);
		playlistPath = resolvePlaylistPath(playlistPath);
		log.debug("\n{}", Files.readString(playlistPath));
	}

	@SneakyThrows
	@ShellMethod("Show a playlist backup's content.")
	public void catb(Path backupPath) {
		throwIfInvalidBackupPlPath(backupPath);
		backupPath = resolveBackupPath(backupPath);
		log.debug("\n{}", Files.readString(backupPath));
	}

	@ShellMethod(value = "Copy a playlist.")
	public void cp(Path sourcePlaylist, Path destinationPlaylist) throws IOException {
		throwIfInvalidPlPath(sourcePlaylist);
		destinationPlaylist = playlistsFilesRepository.cp(sourcePlaylist, destinationPlaylist);
		log.debug("\nCopied from {} to {}", sourcePlaylist, destinationPlaylist);
	}

	@ShellMethod(value = "Move a playlist.")
	public void mv(Path sourcePlaylist, Path destinationPlaylist) throws IOException {
		throwIfInvalidPlPath(sourcePlaylist);
		destinationPlaylist = playlistsFilesRepository.mv(sourcePlaylist, destinationPlaylist);
		log.debug("\nMoved from {} to {}", sourcePlaylist, destinationPlaylist);
	}

	@ShellMethod(value = "Remove a playlist.")
	public void rm(Path playlistPath) throws IOException {
		throwIfInvalidPlPath(playlistPath);
		if (playlistsFilesRepository.rm(playlistPath)) {
			log.debug("\nRemoved {}", playlistPath);
		} else {
			log.debug("\nCouldn't remove {}", playlistPath);
		}
	}

	@ShellMethod(value = "Remove a playlist's backup.")
	public void rmb(Path backupPath) throws IOException {
		throwIfInvalidBackupPlPath(backupPath);
		if (backupFilesRepository.rm(backupPath)) {
			log.debug("\nRemoved {}", backupPath);
		} else {
			log.debug("\nCouldn't remove {}", backupPath);
		}
	}

	@ShellMethod(value = "Remove \"found\" playlists.", key = "rmf")
	public void rmFound() throws IOException {
		throwIfInvalid(PLAYLISTS);
		log.debug("\n{}", playlistsFilesRepository.rmPathFixes(true));
	}

	@ShellMethod(value = "Remove \"not found\" playlists.", key = "rmnf")
	public void rmNotFound() throws IOException {
		throwIfInvalid(PLAYLISTS);
		log.debug("\n{}", playlistsFilesRepository.rmPathFixes(false));
	}

	private Path resolvePlaylistPath(Path playlistPath) {
		return appPaths.resolvePlaylistPath(playlistPath);
	}

	private void throwIfInvalidBackupPlPath(Path backupPlPath) {
		contextPathsValidator().addBackup(backupPlPath).throwIfInvalid();
	}

	private Path resolveBackupPath(Path backupPath) {
		return appPaths.resolveBackupPath(backupPath);
	}
}

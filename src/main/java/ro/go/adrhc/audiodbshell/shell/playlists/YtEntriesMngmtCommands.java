package ro.go.adrhc.audiodbshell.shell.playlists;

import lombok.extern.slf4j.Slf4j;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import ro.go.adrhc.audiodbshell.shell.AbstractCommands;
import ro.go.adrhc.audiodbshell.shell.playlists.domain.YouTubeLocationsAware;
import ro.go.adrhc.audiodbshell.shell.playlists.domain.YouTubeMusicLocations;
import ro.go.adrhc.audiodbshell.shell.playlists.domain.YouTubeVideoLocations;
import ro.go.adrhc.audiodbshell.shell.playlists.domain.YouTubeVideoPlLocations;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.playlists.PlaylistStorageOutcome;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YtVideoPlLocation;
import ro.go.adrhc.audiomanager.managers.YtEntriesManager;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.FileSystemIndex;

import java.io.IOException;
import java.net.URI;

import static org.springframework.shell.standard.ShellOption.NULL;
import static ro.go.adrhc.audiomanager.config.context.AppPathType.PLAYLISTS;
import static ro.go.adrhc.audiomanager.util.LogUtils.log;

@ShellComponent
@Slf4j
public class YtEntriesMngmtCommands extends AbstractCommands {
	private final YtEntriesManager ytEntriesManager;

	public YtEntriesMngmtCommands(AppPaths appPaths,
			FileSystemIndex<URI, AudioMetadata> amIndexRepository,
			YtEntriesManager ytEntriesManager) {
		super(appPaths, amIndexRepository);
		this.ytEntriesManager = ytEntriesManager;
	}

	@ShellMethod(value = "Replace youtube URLs with local paths (keep not found).",
			key = {"switchyttodisk", "yt-uri-to-local-paths"})
	public void switchYtToDiskLocation(
			@ShellOption(value = "--playlist-path") DiskLocation plLocation)
			throws IOException {
		throwIfInvalidPlDiskLocation(plLocation);
		PlaylistStorageOutcome storageOutcome = ytEntriesManager
				.switchYtToDiskLocation(plLocation);
		log(storageOutcome);
	}

	@ShellMethod(key = {"linktoytpl"},
			value = "Create a m3u8 file containing a reference to a YouTube playlist (i.e. playlist link).")
	public void createLinkToYtPlaylist(
			@ShellOption(help = "YouTube video-playlist URI, URL or code",
					value = "--ytPlUrlOrUriOrCode") YtVideoPlLocation ytVideoPlLocation,
			@ShellOption(defaultValue = NULL,
					help = "the path of the m3u8 file to create",
					value = "--link-path") DiskLocation linkLocation) {
		throwIfInvalid(PLAYLISTS);
		PlaylistStorageOutcome storageOutcome = (linkLocation != null ?
				ytEntriesManager.createLinkToYtPlaylist(ytVideoPlLocation, linkLocation) :
				ytEntriesManager.createLinkToYtPlaylist(ytVideoPlLocation));
		log(storageOutcome);
	}

	@ShellMethod(key = {"addytpl"}, value = "Add YouTube Video playlists links/references to a playlist.")
	public void addYtPlaylists(
			@ShellOption(help = "playlist where to add to", value = "--playlist-path") DiskLocation plLocation,
			@ShellOption(help = "comma separated list of YouTube video-playlist URI, URL or code",
					value = "--ytPlUrlOrUriOrCodes") YouTubeVideoPlLocations ytVideoPlLocations) {
		appendYtMedia(plLocation, ytVideoPlLocations);
	}

	@ShellMethod(key = {"addytv"}, value = "Add YouTube Video items to a playlist.")
	public void addYtVideos(
			@ShellOption(help = "playlist to add to", value = "--playlist-path") DiskLocation plLocation,
			@ShellOption(help = "comma separated list of YouTube video URI, URL or code",
					value = "--ytVideoUrlOrUriOrCodes") YouTubeVideoLocations ytVideoLocations) {
		appendYtMedia(plLocation, ytVideoLocations);
	}

	@ShellMethod(key = {"addytm"}, value = "Add YouTube Music items to a playlist.")
	public void addYtMusic(
			@ShellOption(help = "playlist to add to", value = "--playlist-path") DiskLocation plLocation,
			@ShellOption(help = "comma separated list of YouTube music URI, URL or code",
					value = "--ytMusicUrlOrUriOrCodes") YouTubeMusicLocations ytMusicLocations) {
		appendYtMedia(plLocation, ytMusicLocations);
	}

	@ShellMethod(key = {"getytpl"}, value = "Download a YouTube playlist (i.e. copy).")
	public void downloadYtPlaylist(
			@ShellOption(help = "YouTube video-playlist URI, URL or code",
					value = "--ytPlUrlOrUriOrCode") YtVideoPlLocation ytVideoPlLocation,
			@ShellOption(defaultValue = NULL, value = "--playlist-path") DiskLocation plLocation) {
		throwIfInvalid(PLAYLISTS);
		PlaylistStorageOutcome storageOutcome = (plLocation != null ?
				ytEntriesManager.downloadYtPlaylist(ytVideoPlLocation, plLocation) :
				ytEntriesManager.downloadYtPlaylist(ytVideoPlLocation));
		log(storageOutcome);
	}

	@ShellMethod(key = {"getytmliked"}, value = "Download YouTube Music liked playlist.")
	public void downloadYtLikedMusicPlaylist(@ShellOption(defaultValue = NULL,
			value = "--liked-playlist-path") DiskLocation likedPlLocation) {
		throwIfInvalid(PLAYLISTS);
		PlaylistStorageOutcome storageOutcome = (likedPlLocation != null ?
				ytEntriesManager.downloadYtLikedMusicPlaylist(likedPlLocation) :
				ytEntriesManager.downloadYtLikedMusicPlaylist());
		log(storageOutcome);
	}

	private void appendYtMedia(DiskLocation plLocation, YouTubeLocationsAware ytLocationsAware) {
		throwIfInvalid(PLAYLISTS);
		PlaylistStorageOutcome outcome = ytEntriesManager
				.appendYtMedia(plLocation, ytLocationsAware.locations());
		if (outcome.stored()) {
			log.debug("\n{} added to {}", ytLocationsAware, plLocation.rawPath());
		} else {
			log.warn("\nPlaylist {} wasn't stored!", plLocation.rawPath());
		}
	}
}

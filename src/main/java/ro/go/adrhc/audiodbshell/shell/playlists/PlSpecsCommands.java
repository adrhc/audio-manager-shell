package ro.go.adrhc.audiodbshell.shell.playlists;

import lombok.extern.slf4j.Slf4j;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import ro.go.adrhc.audiodbshell.shell.AbstractCommands;
import ro.go.adrhc.audiomanager.config.MainProperties;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.config.plspec.PlaylistSpec;
import ro.go.adrhc.audiomanager.managers.PlSpecsHandlingManager;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.FileSystemIndex;

import java.net.URI;

import static org.springframework.shell.standard.ShellOption.NULL;
import static org.springframework.util.StringUtils.hasText;
import static ro.go.adrhc.audiomanager.config.context.AppPathType.PLAYLISTS;
import static ro.go.adrhc.audiomanager.util.LogUtils.log;

@ShellComponent
@Slf4j
public class PlSpecsCommands extends AbstractCommands {
	private final MainProperties appProperties;
	private final PlSpecsHandlingManager plSpecsHandlingManager;

	public PlSpecsCommands(AppPaths appPaths,
			FileSystemIndex<URI, AudioMetadata> amIndexRepository,
			MainProperties appProperties,
			PlSpecsHandlingManager plSpecsHandlingManager) {
		super(appPaths, amIndexRepository);
		this.appProperties = appProperties;
		this.plSpecsHandlingManager = plSpecsHandlingManager;
	}


	@ShellMethod(key = {"showplspec"},
			value = "Print the designated or all otherwise, playlist specification.")
	public void showPlaylistSpec(@ShellOption(defaultValue = NULL) String name) {
		if (hasText(name)) {
			log.debug("\n{}", appProperties.getPlaylistSpec(name));
		} else {
			appProperties.getSpecNames().forEach(spec ->
					log.debug("\n** {} **\n{}", spec, appProperties.getPlaylistSpec(spec)));
		}
	}

	@ShellMethod(key = {"getplbyspec"},
			value = "Create a playlist according to the designated specification.")
	public void createPlaylistsBySpecs(
			@ShellOption(defaultValue = NULL, value = "--name") PlaylistSpec plSpec) {
		throwIfInvalid(PLAYLISTS);
		if (plSpec == null) {
			plSpecsHandlingManager.createPlFromSpecs();
			log.debug("\nPlaylists creation completed!");
		} else {
			log(plSpecsHandlingManager.createPlFromSpec(plSpec));
		}
	}
}

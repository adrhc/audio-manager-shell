package ro.go.adrhc.audiodbshell.shell.playlists;

import lombok.extern.slf4j.Slf4j;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import ro.go.adrhc.audiodbshell.shell.AbstractPlaylistsBrowser;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.datasources.disk.playlists.DiskPlaylistsRepository;
import ro.go.adrhc.audiomanager.util.stringifier.PlaylistsStringifier;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.FileSystemIndex;

import java.io.IOException;
import java.net.URI;

import static ro.go.adrhc.audiomanager.config.context.AppPathType.PLAYLISTS;

@ShellComponent
@Slf4j
public class PlaylistsBrowseCommands extends AbstractPlaylistsBrowser {
	public PlaylistsBrowseCommands(AppPaths appPaths,
			FileSystemIndex<URI, AudioMetadata> amIndexRepository,
			DiskPlaylistsRepository diskPlaylistsRepository,
			PlaylistsStringifier playlistsStringifier) {
		super(appPaths, amIndexRepository,
				diskPlaylistsRepository, playlistsStringifier);
	}

	@ShellMethod(value = "List all playlists backups.",
			key = {"llb", "list-backups"})
	public void lsBackups() throws IOException {
		showBackups();
	}

	/**
	 * see <a href="https://docs.spring.io/spring-shell/docs/2.0.0.RELEASE/reference/htmlsingle/#_special_handling_of_boolean_parameters">...</a>
	 */
	@ShellMethod(value = "List all playlists, including \"found\" and \"not found\" ones.",
			key = {"ll", "list-playlists"})
	public void lsPlaylists() throws IOException {
		showPlaylists();
	}

	@ShellMethod(value = "List only the \"found\" playlists.",
			key = {"llff", "list-found-fixes"})
	public void lsFound() throws IOException {
		showPlaylists(PLAYLISTS, diskPlaylistsRepository.getFoundOnly());
	}

	@ShellMethod(value = "List only the \"not found\" playlists.",
			key = {"llnf", "list-not-found-fixes"})
	public void lsNotFound() throws IOException {
		showPlaylists(PLAYLISTS, diskPlaylistsRepository.getNotFoundOnly());
	}

	/**
	 * see <a href="https://docs.spring.io/spring-shell/docs/2.0.0.RELEASE/reference/htmlsingle/#_special_handling_of_boolean_parameters">...</a>
	 */
	@ShellMethod(value = "List only the \"found\" and \"not found\" playlists.",
			key = {"llaf", "list-all-fixes"})
	public void lsFoundAndNotFound() throws IOException {
		showPlaylists(PLAYLISTS, diskPlaylistsRepository.getFoundAndNotFound());
	}
}

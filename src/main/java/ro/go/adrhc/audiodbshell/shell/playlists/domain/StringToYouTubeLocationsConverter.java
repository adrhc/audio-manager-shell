package ro.go.adrhc.audiodbshell.shell.playlists.domain;

import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.GenericConverter;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YtVideoPlLocation;

import java.util.Set;

import static ro.go.adrhc.audiomanager.util.YtLocationParser.*;

@Component
public class StringToYouTubeLocationsConverter implements GenericConverter {
	@Override
	public Set<ConvertiblePair> getConvertibleTypes() {
		return Set.of(new ConvertiblePair(String.class, YouTubeMusicLocations.class),
				new ConvertiblePair(String.class, YouTubeVideoLocations.class),
				new ConvertiblePair(String.class, YouTubeVideoPlLocations.class),
				new ConvertiblePair(String.class, YtVideoPlLocation.class));
	}

	@Override
	public Object convert(Object ytUrlOrUriOrCodes, TypeDescriptor sourceType,
			TypeDescriptor targetType) {
		if (targetType.getType() == YouTubeMusicLocations.class) {
			return new YouTubeMusicLocations(parseMusic((String) ytUrlOrUriOrCodes));
		} else if (targetType.getType() == YouTubeVideoLocations.class) {
			return new YouTubeVideoLocations(parseVideos((String) ytUrlOrUriOrCodes));
		} else if (targetType.getType() == YouTubeVideoPlLocations.class) {
			return new YouTubeVideoPlLocations(parseVideoPls((String) ytUrlOrUriOrCodes));
		} else if (targetType.getType() == YtVideoPlLocation.class) {
			return YtVideoPlLocation.of((String) ytUrlOrUriOrCodes);
		}
		throw new UnsupportedOperationException();
	}
}

package ro.go.adrhc.audiodbshell.datasources.disk;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.datasources.disk.PlEntrySongQuery;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.PlEntryLocationPairs;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.SongQueryLocationPairs;
import ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory;
import ro.go.adrhc.audiomanager.datasources.domain.SongQuery;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.util.fn.BiFunctionUtils;
import ro.go.adrhc.util.pair.Pair;

import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.springframework.util.StringUtils.hasText;
import static ro.go.adrhc.audiodbshell.domain.TestData.INDEX;
import static ro.go.adrhc.audiodbshell.domain.TestData.YOUTUBE_MP3_PATHS;

@Component
@RequiredArgsConstructor
public class DiskLocationsRepositoryStub {
	protected final AppPaths appPaths;

	public boolean isValid(DiskLocation diskLocation) {
		Path diskLocPath = diskLocation.path();
		return YOUTUBE_MP3_PATHS.stream().anyMatch(
				path -> diskLocPath.endsWith(path) || path.endsWith(diskLocPath));
	}

	public List<DiskLocation> findAllMatches(SongQuery query) {
		return findByTitle(query.title()).map(List::of).orElseGet(List::of);
	}

	public PlEntryLocationPairs findBestPlEntriesMatches(PlaylistEntries entries) {
		return entries
				.map(e -> findByTitle(e.title()).map(BiFunctionUtils.curry(Pair::new, e)))
				.flatMap(Optional::stream)
				.collect(PlEntryLocationPairs::empty, PlEntryLocationPairs::put,
						PlEntryLocationPairs::putAll);
	}

	public SongQueryLocationPairs<PlEntrySongQuery, DiskLocation> findBestMatches(
			Collection<PlEntrySongQuery> queries) {
		return queries.stream()
				.map(q -> findByTitle(q.title()).map(BiFunctionUtils.curry(Pair::new, q)))
				.flatMap(Optional::stream)
				.collect(SongQueryLocationPairs::empty, SongQueryLocationPairs::put,
						SongQueryLocationPairs::putAll);
	}

	private Optional<DiskLocation> findByTitle(String title) {
		if (!hasText(title)) {
			return Optional.empty();
		}
		String relativeSongPath = INDEX.get(title);
		if (hasText(relativeSongPath)) {
			DiskLocation songLocation = DiskLocationFactory.createDiskLocation(
					appPaths.getSongsPath().resolve(Path.of(relativeSongPath)));
			return Optional.of(songLocation);
		} else {
			return Optional.empty();
		}
	}
}

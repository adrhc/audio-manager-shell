package ro.go.adrhc.audiodbshell.datasources.youtube.metadata.ytsearch;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.datasources.domain.SongQuery;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;

import java.util.List;

import static org.springframework.util.StringUtils.hasText;
import static ro.go.adrhc.audiodbshell.domain.TestData.YT_VIDEOS;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory.ytVideoLocation;

@Component
@RequiredArgsConstructor
public class YtSearchMetadataProviderStub {
	protected final AppPaths appPaths;

	public List<YouTubeMetadata> findAllWebVideoMatches(SongQuery songQuery) {
		return findByTitle(songQuery.title());
	}

	protected List<YouTubeMetadata> findByTitle(String title) {
		if (!hasText(title)) {
			return List.of();
		}
		return YT_VIDEOS.entrySet().stream()
				.filter(e -> e.getValue().contains(title))
				.map(e -> youTubeMetadata(e.getKey(), e.getValue()))
				.toList();
	}

	private static YouTubeMetadata youTubeMetadata(String ytId, String ytTitle) {
		return YouTubeMetadata.of(ytVideoLocation(ytId), ytTitle);
	}
}

package ro.go.adrhc.audiodbshell.datasources;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.shell.Shell;
import ro.go.adrhc.audiodbshell.ExcludeShellAutoConfiguration;
import ro.go.adrhc.audiodbshell.datasources.disk.DiskLocationsRepositoryStub;
import ro.go.adrhc.audiodbshell.datasources.youtube.metadata.ytsearch.YtSearchMetadataProviderStub;
import ro.go.adrhc.audiomanager.datasources.disk.locations.DiskLocationsRepository;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.domain.SongQuery;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.datasources.youtube.locations.YtVideoPlContentLocationsProvider;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytliked.YtLikedMusicMetadataProvider;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytplaylists.YtVideoPlMetadataProvider;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsearch.YtSearchMetadataProvider;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsongsandvideos.youtubedl.YtDlSongsAndVideosMetadataProvider;

import java.io.IOException;
import java.util.Collection;

import static java.lang.Integer.MAX_VALUE;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyCollection;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.reset;
import static ro.go.adrhc.audiodbshell.domain.location.LocationsGenerator.*;

@SpringBootTest
@ExcludeShellAutoConfiguration
@MockBean(classes = {Shell.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class AbstractStubsBasedTest {
	@MockBean
	protected YtLikedMusicMetadataProvider ytLikedMusicMetadataProvider;
	@MockBean
	protected YtVideoPlContentLocationsProvider ytVideoPlContentLocationsProvider;
	@SpyBean
	protected YtDlSongsAndVideosMetadataProvider ytDlSongsAndVideosMetadataProvider;
	@SpyBean
	protected YtVideoPlMetadataProvider ytVideoPlMetadataProvider;
	@SpyBean
	protected DiskLocationsRepository diskLocationsRepository;
	@SpyBean
	protected YtSearchMetadataProvider ytSearchMetadataProvider;
	@Autowired
	protected DiskLocationsRepositoryStub diskLocationsRepositoryStub;
	@Autowired
	protected YtSearchMetadataProviderStub ytSearchMetadataProviderStub;

	/**
	 * Doesn't work with @BeforeAll! the when-thenReturn is simply not applied.
	 */
	@BeforeEach
	protected void beforeEach() throws IOException {
		BeforEachConfig beforEachConfig = beforEachConfig();
		reset(ytLikedMusicMetadataProvider);
		reset(ytVideoPlContentLocationsProvider);
		reset(ytDlSongsAndVideosMetadataProvider);
		reset(ytVideoPlMetadataProvider);
		reset(diskLocationsRepository);
		doAnswer(invocation -> generateYtMetadata(beforEachConfig.likedMaxSize()))
				.when(ytLikedMusicMetadataProvider).loadMetadata();
		doAnswer(invocation -> createPlContent(beforEachConfig.otherPlMaxSize(),
				invocation.getArgument(0)))
				.when(ytVideoPlContentLocationsProvider).getPlContent(any(YouTubeLocation.class));
		doAnswer(invocation -> ytLocationMetadataPairsOf(invocation.getArgument(0)))
				.when(ytDlSongsAndVideosMetadataProvider).loadMetadata(
						Mockito.<Collection<YouTubeLocation>>any());
		doAnswer(invocation -> ytLocationMetadataPairsOf(invocation.getArgument(0)))
				.when(ytVideoPlMetadataProvider).loadMetadata(
						Mockito.<Collection<YouTubeLocation>>any());
		doAnswer(invocation -> youtubeMp3Content())
				.when(diskLocationsRepository).getContent(YOUTUBE_MP3);
		doAnswer(invocation -> diskLocationsRepositoryStub
				.findBestMatches(invocation.getArgument(0)))
				.when(diskLocationsRepository).findBestMatches(anyCollection());
		doAnswer(invocation -> diskLocationsRepositoryStub
				.findAllMatches(invocation.getArgument(0)))
				.when(diskLocationsRepository).findMany(any(SongQuery.class));
		doAnswer(invocation -> diskLocationsRepositoryStub.isValid(invocation.getArgument(0)))
				.when(diskLocationsRepository).isValid(any(DiskLocation.class));
		doAnswer(invocation -> ytSearchMetadataProviderStub
				.findAllWebVideoMatches(invocation.getArgument(0)))
				.when(ytSearchMetadataProvider).findAllWebVideoMatches(any(SongQuery.class));
	}

	protected BeforEachConfig beforEachConfig() {
		return new BeforEachConfig(MAX_VALUE, MAX_VALUE);
	}

	public record BeforEachConfig(int likedMaxSize, int otherPlMaxSize) {
	}
}

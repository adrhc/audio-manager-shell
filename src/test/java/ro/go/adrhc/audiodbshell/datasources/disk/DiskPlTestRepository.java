package ro.go.adrhc.audiodbshell.datasources.disk;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.playlists.DiskPlaylistsRepository;
import ro.go.adrhc.audiomanager.datasources.disk.playlists.PlaylistStorageOutcome;
import ro.go.adrhc.audiomanager.domain.playlist.Playlist;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;

import java.nio.file.Path;

import static ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory.createDiskLocation;

@Component
@RequiredArgsConstructor
public class DiskPlTestRepository {
	private final DiskPlaylistsRepository diskPlaylistsRepository;

	public PlaylistStorageOutcome storeIfNotEmpty(
			Path path, PlaylistEntries plEntries) {
		return diskPlaylistsRepository.storeIfNotEmpty(
				new Playlist<>(createDiskLocation(path), plEntries));
	}

	@SneakyThrows
	public Playlist<DiskLocation> loadPlaylist(Resource resource) {
		return loadPlaylist(createDiskLocation(resource));
	}

	public Playlist<DiskLocation> loadPlaylist(DiskLocation plLocation) {
		return diskPlaylistsRepository.load(plLocation);
	}

	@SneakyThrows
	public PlaylistEntries loadEntries(Resource resource) {
		return loadPlaylist(resource).entries();
	}

	public PlaylistEntries loadEntries(DiskLocation plLocation) {
		return loadPlaylist(plLocation).entries();
	}
}

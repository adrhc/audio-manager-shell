package ro.go.adrhc.audiodbshell.config;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.shell.Shell;
import ro.go.adrhc.audiodbshell.ExcludeShellAutoConfiguration;
import ro.go.adrhc.audiomanager.config.MainProperties;
import ro.go.adrhc.audiomanager.config.plspec.PlaylistSpec;
import ro.go.adrhc.audiomanager.domain.location.Location;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ExcludeShellAutoConfiguration
@MockBean(classes = {Shell.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class AudioManagerPropertiesTest {
	@Autowired
	private MainProperties appProperties;

	@Test
	void getPlaylistSpec() {
		PlaylistSpec liked = appProperties.getPlaylistSpec("liked");
		assertThat(liked.filename()).isEqualTo("lm-all-and-local.copy.m3u8");
		assertThat(liked.locations()).map(Location::toString).contains("ytmusic:playlist:LM",
				"DISK: Youtube_mp3", "youtube:playlist:PLJw1EkQJl1znlNjw8B0uzGAeOEjpTh3Ot");
	}
}
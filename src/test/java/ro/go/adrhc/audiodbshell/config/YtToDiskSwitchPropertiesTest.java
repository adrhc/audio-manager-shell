package ro.go.adrhc.audiodbshell.config;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.shell.Shell;
import ro.go.adrhc.audiodbshell.ExcludeShellAutoConfiguration;
import ro.go.adrhc.audiomanager.config.YtToDiskSwitchProperties;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.domain.playlist.Playlist;

import static org.assertj.core.api.Assertions.assertThat;
import static ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory.createPlLocation;
import static ro.go.adrhc.audiomanager.domain.playlist.PlaylistFactory.of;

@SpringBootTest
@ExcludeShellAutoConfiguration
@MockBean(classes = {Shell.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class YtToDiskSwitchPropertiesTest {
	@Autowired
	private YtToDiskSwitchProperties properties;

	@Test
	void toSwitchedPlLocation() {
		Playlist<DiskLocation> playlist = of(createPlLocation("some-yt-pl.copy"));
		playlist = properties.toLocalPlLocation(playlist);
		assertThat(playlist.location().name()).isEqualTo("some-yt-pl.local");
	}
}
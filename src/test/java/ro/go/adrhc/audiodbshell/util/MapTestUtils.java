package ro.go.adrhc.audiodbshell.util;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static ro.go.adrhc.util.fn.BiConsumerUtils.unaryToBiOper;
import static ro.go.adrhc.util.fn.BiFunctionUtils.toBiFn;

public class MapTestUtils {
	public static <K, V> void addEntry(Map<K, V> map, Map.Entry<K, V> entry) {
		map.put(entry.getKey(), entry.getValue());
	}

	public static <K, V> Map<K, V> mapOf(Object... values) {
		MapEntry<K, V> mapEntry = new MapEntry<>();
		return Arrays.stream(values)
				.map(mapEntry::setKeyOrValue)
				.filter(MapEntry::isReady)
				.map(MapEntry::reset)
				.reduce(new HashMap<>(), toBiFn(MapTestUtils::addEntry),
						unaryToBiOper(Map::putAll));
	}

	@Accessors(fluent = true)
	@Getter
	@Setter
	private static class MapEntry<K, V> {
		private K key;
		private V value;

		public boolean isReady() {
			return key != null && value != null;
		}

		public MapEntry<K, V> setKeyOrValue(Object keyOrValue) {
			if (key == null) {
				key = (K) keyOrValue;
			} else {
				value = (V) keyOrValue;
			}
			return this;
		}

		public AbstractMap.SimpleEntry<K, V> reset() {
			AbstractMap.SimpleEntry<K, V> simpleEntry = new AbstractMap.SimpleEntry<>(key, value);
			key = null;
			value = null;
			return simpleEntry;
		}
	}
}

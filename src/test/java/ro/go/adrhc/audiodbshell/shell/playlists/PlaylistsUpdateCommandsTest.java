package ro.go.adrhc.audiodbshell.shell.playlists;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.beans.factory.annotation.Autowired;
import ro.go.adrhc.audiodbshell.shell.AbstractCommandsTest;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ro.go.adrhc.audiodbshell.domain.location.LocationsGenerator.YOUTUBE_MP3_PATH;
import static ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory.createDiskLocation;

/**
 * appPaths.setPlaylistsPath(tempDir) doesn't work with TestInstance.Lifecycle.PER_CLASS!
 */
@Slf4j
class PlaylistsUpdateCommandsTest extends AbstractCommandsTest {
	@Autowired
	private PlaylistsUpdateCommands playlistsUpdateCommands;

	@Test
	void createPlaylistFromDiskLocation(@TempDir Path tempDir) throws IOException {
		observableAppPaths.setPlaylistsPath(tempDir);
		playlistsUpdateCommands.createPlFromDirectory(
				createDiskLocation(YOUTUBE_MP3_PATH));
		DiskLocation plLocation = createDiskLocation(
				tempDir.resolve(YOUTUBE_MP3_PATH.getFileName() + ".m3u8"));
		assertTrue(Files.isRegularFile(plLocation.path()));
		PlaylistEntries entries = playlistHelper.loadEntries(plLocation);
		assertThat(entries).isNotEmpty();
		assertThat(entries).map(PlaylistEntry::title).containsSubsequence(
				"Alabina (Original Version) - Alabina, Charles Burbank, Pedro Maza Luaces, Samy Goz",
				"Beloved - Sweet Harmony", "Boney M - Rasputin", "Dire Straits - Brothers In Arms");
	}
}
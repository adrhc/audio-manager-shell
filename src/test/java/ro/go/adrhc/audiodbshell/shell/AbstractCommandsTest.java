package ro.go.adrhc.audiodbshell.shell;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import ro.go.adrhc.audiodbshell.datasources.AbstractStubsBasedTest;
import ro.go.adrhc.audiodbshell.datasources.disk.DiskPlTestRepository;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.config.context.ObservableAppPaths;

import java.io.IOException;

public abstract class AbstractCommandsTest extends AbstractStubsBasedTest {
	@Autowired
	protected ObservableAppPaths observableAppPaths;
	protected AppPaths appPathsBackup;
	@Autowired
	protected DiskPlTestRepository playlistHelper;

	@BeforeEach
	protected void beforeEach() throws IOException {
		super.beforeEach();
		appPathsBackup = observableAppPaths.cloneAppPaths();
	}

	@AfterEach
	protected void afterEach() {
		observableAppPaths.copy(appPathsBackup);
	}
}

package ro.go.adrhc.audiodbshell.shell.playlists;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.beans.factory.annotation.Autowired;
import ro.go.adrhc.audiodbshell.shell.AbstractCommandsTest;
import ro.go.adrhc.audiodbshell.shell.playlists.domain.YouTubeMusicLocations;
import ro.go.adrhc.audiodbshell.shell.playlists.domain.YouTubeVideoLocations;
import ro.go.adrhc.audiodbshell.shell.playlists.domain.YouTubeVideoPlLocations;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YtVideoPlLocation;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlEntryFactory;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;

import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;
import static ro.go.adrhc.audiodbshell.domain.TestData.*;
import static ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory.createDiskLocation;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory.ytVideoPlLocation;
import static ro.go.adrhc.lib.m3u8.domain.Extinf.UNKNOWN_SECONDS;

@Slf4j
class YtEntriesManagementCommandsTest extends AbstractCommandsTest {
	@Autowired
	private YtEntriesMngmtCommands updateCommands;

	@Test
	void createLinkToYtPlaylist(@TempDir Path tempDir) {
		observableAppPaths.setPlaylistsPath(tempDir);
		updateCommands.createLinkToYtPlaylist(YtVideoPlLocation.of(YT_MALUKAH_PL_ID), null);
		DiskLocation plLocation = createDiskLocation(tempDir.resolve(
				"%s.link.%s.m3u8".formatted(YT_MALUKAH_PL_TITLE, YT_MALUKAH_PL_ID)));
		PlaylistEntries m3u8Records = playlistHelper.loadEntries(plLocation);
		// example entry (aka playlist link):
		// #EXTINF:-1,Malukah - Video Game Covers & Originals
		// youtube:playlist:PL6B14C3B2024B1CF9
		assertThat(m3u8Records).containsExactly(
				PlEntryFactory.of(ytVideoPlLocation(YT_MALUKAH_PL_ID))
						.title(YT_MALUKAH_PL_TITLE).duration(UNKNOWN_SECONDS));
	}

	@Test
	void addYtVideos(@TempDir Path tempDir) {
		observableAppPaths.setPlaylistsPath(tempDir);
		updateCommands.addYtVideos(createDiskLocation("addYtVideos.m3u8"),
				YouTubeVideoLocations.of(YT_MALUKAH_PL_ITEM1_ID, YT_MALUKAH_PL_ITEM2_ID));
		DiskLocation plLocation = createDiskLocation(tempDir.resolve("addYtVideos.m3u8"));
		PlaylistEntries entries = playlistHelper.loadEntries(plLocation);
		assertThat(entries).map(PlaylistEntry::title)
				.containsExactly(YT_MALUKAH_PL_ITEM1_TITLE, YT_MALUKAH_PL_ITEM2_TITLE);
	}

	@Test
	void addYtMusic(@TempDir Path tempDir) {
		observableAppPaths.setPlaylistsPath(tempDir);
		DiskLocation plLocation = createDiskLocation(tempDir.resolve("addYtMusic.m3u8"));
		updateCommands.addYtMusic(plLocation, YouTubeMusicLocations.of(
				"jsWW72FmetQ", "vxa8ShIm9yw", "fPZxY0Vw1qM", "T8UMtKw6Uho"));
		PlaylistEntries entries = playlistHelper.loadEntries(plLocation);
		assertThat(entries).map(PlaylistEntry::title)
				.containsExactly("Moonlight Ride - Bluey Moon, Isaac Chambers",
						"Enemy (From the series Arcane League of Legends) - Arcane, Imagine Dragons, League Of Legends",
						"Cantec de dragoste - Z.o.b.",
						"Bakerman - Laid Back");
	}

	@Test
	void addYtPlaylists(@TempDir Path tempDir) {
		observableAppPaths.setPlaylistsPath(tempDir);
		DiskLocation plLocation = createDiskLocation(tempDir.resolve("addYtPlaylists.m3u8"));
		updateCommands.addYtPlaylists(plLocation,
				YouTubeVideoPlLocations.of(YT_MALUKAH_PL_ID, YT_MORROWIND_PL_ID));
		PlaylistEntries entries = playlistHelper.loadEntries(plLocation);
		// example entry (aka playlist link):
		// #EXTINF:-1,Malukah - Video Game Covers & Originals
		// youtube:playlist:PL6B14C3B2024B1CF9
		assertThat(entries).map(PlaylistEntry::title)
				.containsExactly(YT_MALUKAH_PL_TITLE, YT_MORROWIND_PL_TITLE);
	}
}
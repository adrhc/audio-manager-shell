package ro.go.adrhc.audiodbshell;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.shell.boot.*;

@EnableAutoConfiguration(exclude = {
		SpringShellAutoConfiguration.class,
		JLineShellAutoConfiguration.class,
		LineReaderAutoConfiguration.class,
		CompleterAutoConfiguration.class,
		ShellRunnerAutoConfiguration.class})
public @interface ExcludeShellAutoConfiguration {
}

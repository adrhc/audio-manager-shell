package ro.go.adrhc.audiodbshell.domain.location;

import lombok.extern.slf4j.Slf4j;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocationContent;
import ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YtLocationMetadataPairs;
import ro.go.adrhc.util.collection.SetUtils;

import java.nio.file.Path;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static org.springframework.util.StringUtils.hasText;
import static ro.go.adrhc.audiodbshell.domain.TestData.*;
import static ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory.createDiskLocations;
import static ro.go.adrhc.util.collection.ListUtils.limit;
import static ro.go.adrhc.util.fn.BiPredicateUtils.toBiFn;
import static ro.go.adrhc.util.fn.BiPredicateUtils.toBiOper;

@Slf4j
public class LocationsGenerator {
	public static final Path YOUTUBE_MP3_PATH = Path.of("Youtube_mp3");
	public static final DiskLocation YOUTUBE_MP3 = DiskLocationFactory.createDiskLocation(
			YOUTUBE_MP3_PATH);
	private static final Map<String, Supplier<List<YouTubeLocation>>> YT_PL_ITEMS_PROVIDERS = Map.of(
			YT_MALUKAH_PL_ID, LocationsGenerator::createMalukahPlContent,
			YT_MORROWIND_PL_ID, LocationsGenerator::createMorrowindPlContent,
			YT_LIKES_ALTERNATIVE_PL_ID, LocationsGenerator::createLikesAlternative
	);

	public static DiskLocationContent youtubeMp3Content() {
		return new DiskLocationContent(YOUTUBE_MP3, youtubeMp3Paths());
	}

	public static List<DiskLocation> youtubeMp3Paths() {
		return createDiskLocations(YOUTUBE_MP3_PATHS);
	}

	public static YtLocationMetadataPairs ytLocationMetadataPairsOf(
			Collection<YouTubeLocation> ytPlLocations) {
		return YtLocationMetadataPairs.of(ytMetadataOf(ytPlLocations));
	}

	public static Set<YouTubeMetadata> generateYtMetadata(int maxItems) {
		return ytMetadataOf(createYtLikedLocations(maxItems));
	}

	public static Set<YouTubeMetadata> ytMetadataOf(Collection<YouTubeLocation> ytPlLocations) {
		return ytPlLocations.stream().distinct()
				.map(LocationsGenerator::ytMetadataOf)
				.filter(Optional::isPresent)
				.map(Optional::get)
				.collect(Collectors.toSet());
	}

	public static List<YouTubeLocation> createPlContent(int maxItems,
			YouTubeLocation ytPlLocation) {
		return limit(maxItems, YT_PL_ITEMS_PROVIDERS.get(ytPlLocation.code()).get());
	}

	public static List<YouTubeLocation> createMorrowindPlContent() {
		return ytIdsToYtVideoLocations(new ArrayList<>(), MORROWIND_IDS);
	}

	public static List<YouTubeLocation> createMalukahPlContent() {
		return ytIdsToYtVideoLocations(new ArrayList<>(), MALUKAH_IDS);
	}

	public static Set<YouTubeLocation> createYtLikedLocations(int maxItems) {
		return SetUtils.limit(maxItems, createYtLikedLocations());
	}

	public static Set<YouTubeLocation> createYtLikedLocations() {
		return ytIdsToYtVideoLocations(new TreeSet<>(), YT_LIKES_IDS);
	}

	private static List<YouTubeLocation> createLikesAlternative() {
		return ytIdsToYtVideoLocations(new ArrayList<>(), LIKES_ALTERNATIVE_IDS);
	}

	private static <T extends Collection<YouTubeLocation>> T ytIdsToYtVideoLocations(
			T collection, Collection<String> ytIdes) {
		return ytIdes.stream()
				.map(YouTubeLocationFactory::ytVideoLocation)
				.reduce(collection, toBiFn(Collection::add), toBiOper(Collection::addAll));
	}

	private static Optional<YouTubeMetadata> ytMetadataOf(YouTubeLocation ytLocation) {
		String title = YT_PLAYLISTS.getOrDefault(ytLocation.code(),
				YT_VIDEOS.get(ytLocation.code()));
		if (!hasText(title)) {
			log.debug("\nLocation title is missing for {}.", ytLocation);
			return Optional.empty();
		}
		return Optional.of(YouTubeMetadata.of(ytLocation, title));
	}
}

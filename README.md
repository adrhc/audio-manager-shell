# audio db shell

The playlists can contain references to:  
- local files
- YouTube videos or music
- online radios
- URLs

Features:  
- repair, sort, merge, de-duplicate, add YouTube references
- index and search local media
- change from YouTube references to local files
- create bash scripts to download missing YouTube media
- define playlist specifications describing directories and other playlists, local or YouTube, to incorporate 
- create playlists by specification 
- playlist files management (i.e. one doesn't have to exit to e.g. view, rename, etc a file)

# usage

## bash alias

```bash
alias fixmp3='java20 -jar $HOME/Projects-adrhc/audio-manager/audio-manager-shell/m3u8-playlists-manager/target/playlist-0.0.1-SNAPSHOT.jar --spring.config.additional-location=$HOME/Projects-adrhc/audio-manager/audio-manager-shell/config/'
```

# maintenance

### ERROR: feign.FeignException$Forbidden: [403 Forbidden] during [POST] to ...

It means that "YouTube Music" doesn't work! e.g. YouTube Music -> Liked Songs doesn't work and has
nothing
to do with "YouTube"! e.g. it is NOT related to YouTube -> My Youtube playlists -> Likes Alternative

Solve by fixing music-curl-path: .../config/yt-music-liked-curl.txt.
Verify yt-music-liked-curl.txt validity with YtLikedMusicRestClientIT or with "fix-yt-music-auth"
command.

Update using "CURL (bash)" of YouTube Music -> Library -> Your Likes then RESTART Mopidy!

```./mvnw test -Dtest="YtLikedMusicRestClientIT"```
```./mvnw test -Dtest="YtLikedMusicMetadataProviderIT#isYouTubeMusicAuthValid"```

### Fix yt-music-liked-curl.txt

Copy as "CURL (bash)" into
yt-music-liked-curl.txt the 1st called URL of YouTube Music -> Library -> Your Likes.
Use "fix-yt-music-auth" command to update /var/lib/mopidy/.config/auth.json then restart Mopidy (
Raspberry Pi too).

## video-curl-path: .../config/youtube-video-liked-curl.txt

Go to https://www.youtube.com/playlist?list=LL then copy its bash CURL.

for the moment this is not used

# debugging

## run with spring boot

```bash
./mvnw -DskipTests clean package
./mvnw spring-boot:run --spring.config.additional-location=file:///home/...
```

## run the (spring boot fat) jar

```batch
%homedrive%%homepath%\scoop\apps\openjdk\17.0.1-12\bin\java --enable-preview -jar %homedrive%%homepath%\Projects-adrhc\audio-manager\audio-manager-shell\m3u8-playlists-manager\target\playlist-0.0.1-SNAPSHOT.jar --spring.config.additional-location=file:///%homedrive%%homepath%/Projects-adrhc/audio-manager/audio-manager-shell/config/
```

# libraries

https://www.sauronsoftware.it/projects/jave/download.php

# bash scripts

```bash
mvn install:install-file -DgroupId=it.sauronsoftware -DartifactId=jave -Dversion=1.0.2 -Dpackaging=jar -Dfile=/home/gigi/temp/jave-1.0.2/jave-1.0.2.jar
```

# batch or powershell scripts

```batch
mvn install:install-file -DgroupId="it.sauronsoftware" -DartifactId=jave -Dversion="1.0.2" -Dpackaging=jar -Dfile="%homedrive%%homepath%\Projects-adrhc\jave-1.0.2\jave-1.0.2.jar"
mvn install:install-file -DgroupId="it.sauronsoftware" -DartifactId=jave -Dversion="1.0.2" -Dpackaging=jar -Dfile="jave-1.0.2.jar"
```

# Luke (Lucene)

https://github.com/DmitryKey/luke.git  
Binary releases: lucene-8.11.0.tgz  
https://www.apache.org/dyn/closer.lua/lucene/java/8.11.0/lucene-8.11.0.tgz -> lucene-8.11.0\lucene-8.11.0\luke\luke.sh

# jq

```bash
jq '.. | .videoId? | select(.)' youtube-music-likes.json
((JSONArray) JsonPath.read(json, "$..videoId")).stream().map(Object::toString).collect(Collectors.toSet());
```

# youtube-dl

https://github.com/ytdl-org/youtube-dl plugins:
Get cookies.txt LOCALLY (for Chrome) or cookies.txt (for Firefox)
```./mvnw test -Dtest="YoutubeDlIT"```

# Useful links

https://www.shazam.com/myshazam

# maven commands

mvn archetype:generate -DgroupId=ro.go.adrhc -DartifactId=playlist -DarchetypeArtifactId=maven-archetype-quickstart
mvn archetype:generate -DgroupId=ro.go.adrhc -DartifactId=deduplicator -DarchetypeArtifactId=maven-archetype-quickstart